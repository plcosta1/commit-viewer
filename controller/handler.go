package controller

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"strings"

	"gitlab.com/plcosta1/commit-viewer/commits"
)

func validateGithubURL(rawurl string) (*commits.GithubURL, error) {
	url, err := url.Parse(rawurl)
	if err != nil {
		return nil, commits.ErrInvalidURL
	}
	if url.Hostname() != "github.com" {
		return nil, commits.ErrInvalidURL
	}
	split := strings.Split(url.Path, "/")
	if len(split) < 3 {
		return nil, commits.ErrInvalidURL
	}
	return &commits.GithubURL{Owner: split[1], Repo: split[2]}, nil
}

func validateSHA(hex string) error {
	if hex == "" {
		return nil
	} else if len(hex) != 40 {
		return commits.ErrInvalidSHA
	}
	return nil
}

func tryBoth(ghurl *commits.GithubURL, sinceSHA string) ([]*commits.HTTPCommit, error) {
	clist, err := commits.LogFromAPI(ghurl, sinceSHA)
	if err == nil {
		return clist, nil
	}
	return commits.LogFromClone(ghurl, sinceSHA)
}

func GetCommitsForRepo(w http.ResponseWriter, r *http.Request) {
	urlParam := r.URL.Query().Get("repo")
	sinceParam := r.URL.Query().Get("since")

	ghURL, err := validateGithubURL(urlParam)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		resp, _ := json.Marshal(err)
		w.Write(resp)
		return
	}

	err = validateSHA(sinceParam)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		resp, _ := json.Marshal(err)
		w.Write(resp)
		return
	}

	commitList, err := tryBoth(ghURL, sinceParam)
	if err != nil {
		if errors.Is(err, commits.ErrNotFound) {
			w.WriteHeader(http.StatusBadRequest)
			resp, _ := json.Marshal(err)
			w.Write(resp)
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	response, err := json.Marshal(map[string]interface{}{"commits": commitList})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(response)
}
