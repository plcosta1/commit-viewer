# commit-viewer challenge

Requires [go installed](https://golang.org/doc/install) and [GOPATH set](https://github.com/golang/go/wiki/SettingGOPATH).

Requires [node and npm installed](https://nodejs.org/en/download/).

## Backend portion

Fetch this repository to your GOPATH. 

If you already have go installed the easiest is to:

`go get -u gitlab.com/plcosta1/commit-viewer`

Project dependencies can be installed with [dep](https://github.com/golang/dep) using `dep ensure`

With everything installed you can have the server up and running with:

`go run main.go`

The backend is currently serving on :8080


## Frontend portion

With nodejs and npm installed, install dependencies by running `npm install` inside the frontend folder (where the root package.json sits).

To run the frontend you can simply:

`npm run start`

The frontend serving on port :3000
