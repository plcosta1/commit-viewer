package main

import (
	"log"
	"net/http"
	"strings"

	"github.com/go-chi/chi"
	"github.com/go-chi/cors"
	"gitlab.com/plcosta1/commit-viewer/controller"
)

func allowedOrigins(r *http.Request, origin string) bool {
	return strings.HasPrefix(origin, "http://localhost")
}

func main() {
	r := chi.NewRouter()

	cors := cors.New(cors.Options{
		AllowOriginFunc:  allowedOrigins,
		AllowedMethods:   []string{"GET", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Content-Type"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	})

	r.Use(cors.Handler)
	r.Get("/api/commits", controller.GetCommitsForRepo)

	log.Fatal(http.ListenAndServe(":8080", r))
}
