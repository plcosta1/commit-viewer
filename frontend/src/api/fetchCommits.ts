
const API_URL = "http://localhost:8080";

type SinceSHA = string
type RepoURL = string

const fetchCommits = async (repo: RepoURL, since: SinceSHA): Promise<Commit[]> => {
    const url = new URL(`${API_URL}/api/commits`)
    url.searchParams.append('repo', repo)
    if (since) url.searchParams.append('since', since);
    
    const response = await fetch(url.href)
    if(!response.ok) {
        switch (response.status) {
            case 400:
                return Promise.reject(await response.json())
            case 404:
                return Promise.reject(await response.json())
            default:
        }
        return Promise.reject(response.statusText)
    }

    const { commits } = await response.json()
    return commits;
}

export default fetchCommits;