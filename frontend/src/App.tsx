import React from 'react';
import Header from './components/Header'
import SidebarPage from './components/SidebarPage'
import { CommitListProvider } from './context/CommitsList';
import CommitsViewPage from './page/CommitsView';
import "./base.css"

const App: React.FC = () => {
  return (
    <>
      <Header />
      <SidebarPage>
        <CommitListProvider defaultRepo="https://github.com/src-d/go-git">
          <CommitsViewPage />
        </CommitListProvider>
      </SidebarPage>
    </>
  );
}

export default App;
