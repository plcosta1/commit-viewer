import React from 'react';
import styled from 'styled-components';


const Container = styled.div`
    background-color: #101e35;
    height: 60px;
    display: flex;
    padding: .4rem 1.8rem;
    align-items: center;
    > img {
        height: 2.2rem;
        width: 2.2rem;
    }
`;

const Header = () => {
    return (
        <Container>
            <img src="https://app.codacy.com/legacy/versioned/images/codacy-white.svg" alt="Codacy"/>
        </Container>
    )
}

export default Header;