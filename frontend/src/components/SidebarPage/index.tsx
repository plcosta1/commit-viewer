import React from 'react';
import styled from 'styled-components';
import SidebarEntry from './SidebarEntry';

import DashIcon from './assets/dashboard.svg';
import CommitsIcon from './assets/commits.svg';
import FilesIcon from './assets/files.svg';
import IssuesIcon from './assets/issues.svg';
import PullReqIcon from './assets/pull_requests.svg';
import SecurityIcon from './assets/security.svg';

const Container = styled.div`
    display: flex;
    min-height: calc(100vh - 60px);
`;

const SidebarContainer = styled.div`
    background-color: #edf5fc;
    border-right: 1px solid #dbe6ec;
    width: 5.8rem;
    flex-shrink: 0;
    color: #1d3660;
`;

const SidebarStick = styled.div`
    position: sticky;
    top: 0px;
    padding-top: 1rem;
`;

const PageContainer = styled.div`
    padding: 2.2rem;
    flex-grow: 1;
`;


// Leaving this as a mock logic for navigation
const SidebarEntries: [string, string, boolean][] = [
    [DashIcon, "Dashboard", false],
    [CommitsIcon, "Commits", true],
    [FilesIcon, "Files", false],
    [IssuesIcon, "Issues", false],
    [PullReqIcon, "Pull Requests", false],
    [SecurityIcon, "Security", false],
]

const SidebarPage: React.FC = ({ children }) => {
    return (
        <Container>
            <SidebarContainer>
                <SidebarStick>
                    {SidebarEntries.map(e => {
                        return <SidebarEntry key={e[1]} icon={e[0]} text={e[1]} selected={e[2]} />
                    })}
                </SidebarStick>
            </SidebarContainer>
            <PageContainer>{children}</PageContainer>
        </Container>
    )
}

export default SidebarPage;