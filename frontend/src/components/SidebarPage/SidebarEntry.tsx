import React from 'react';
import styled from 'styled-components';

interface SidebarButtonP {
    selected: boolean
}
const SidebarButton = styled.a<SidebarButtonP>`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    font-size: 0.75rem;

    padding: 0.5rem 0;
    margin: 0.25rem 0;

    text-align: center;
    text-decoration: none;
    color: inherit;
    font-weight: ${({selected}) => selected ? 'bold' : 'normal'};
    img {
        opacity: ${({selected}) => selected ? '1' : '0.6'};
        height: 1.5rem;
        margin-bottom: 0.2rem;
    }
    &:hover{
        font-weight: bold;
        img {
            opacity: 1;
        }
    }
`;


interface SidebarEntryP {
    icon: string,
    text: string,
    selected: boolean,
}
const SidebarEntry: React.FC<SidebarEntryP> = ({icon, text, selected}) => {
    return (
        <SidebarButton href="#" selected={selected}>
            <img src={icon} alt={text}/>
            <span>{text}</span>
        </SidebarButton>
    )
}

export default SidebarEntry;