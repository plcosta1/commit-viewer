import React from 'react';
import styled from 'styled-components';
import CommitRow from './CommitRow';

const TableElem = styled.table`
    width: 100%;
    text-align: left;
    border-top: 1px solid #e5ecf5;
    color: #15294a;
`;

const TableHeadRow = styled.tr`
    color: #7e90b2;
    font-size: 0.75rem;
    > th {
        font-weight: 400;
        padding: .8rem 0.5rem;
        border-bottom: 2px solid #e5ecf5;
    }
`;


const CommitsTable: React.FC<{commits: Commit[]}> = ({ commits }) => {
    return (
        <TableElem cellPadding={0} cellSpacing={0}>
            <thead>
                <TableHeadRow>
                    <th>AUTHOR</th>
                    <th>COMMIT</th>
                    <th>MESSAGE</th>
                    <th>CREATED</th>
                </TableHeadRow>
            </thead>
            <tbody>
                {commits.map(c => <CommitRow key={c.hash} commit={c} />)}
            </tbody>
        </TableElem>
    )
}
export default CommitsTable;