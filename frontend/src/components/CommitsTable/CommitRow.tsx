import React from 'react';
import styled from 'styled-components';
import { formatDistance } from 'date-fns'
import md5 from 'md5';


const TableRow = styled.tr`
    font-size: 0.9rem;
    > td {
        border-bottom: 1px solid #e5ecf5;
        padding: 0.8rem 0.5rem;
    }
    &:nth-child(odd) {
        background-color: #f6f9ff;
    }
`;

const HashCell = styled.td`
    font-family: monospace;
    font-size: 1.2em;
`;

const AuthorCell = styled.td`
    > img {

    vertical-align: middle;
        border-radius: 3rem;
        width: 1.5rem;
        height: 1.5rem;
        margin-right: 0.5rem;
    }
    > span {
        font-weight: 600;
        vertical-align: middle;
    }
`;

const GravatarImage: React.FC<{email: string}> = ({ email }) => {
    const src = `http://www.gravatar.com/avatar/${md5(email)}.jpg?s=50`;
    return <img src={src} alt={email} />
}

const CommitRow: React.FC<{commit: Commit}> = ({ commit }) => {
    return (
        <TableRow>
            <AuthorCell>
                <GravatarImage email={commit.email} />
                <span>{commit.author}</span>
            </AuthorCell>
            <HashCell>{commit.hash.substr(0,8)}</HashCell>
            <td>{commit.message.split('\n')[0]}</td>
            <td>{formatDistance(new Date(commit.when), new Date())}</td>
        </TableRow>
    )
}

export default CommitRow;