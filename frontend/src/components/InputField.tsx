import styled from 'styled-components';

const InputField = styled.input`
    height: 1.8rem;
    min-width: 16rem;
    border-radius: 0.25rem;
    border: 1px solid #e5ecf5;
    padding: 0.2em;
`;

export default InputField;
