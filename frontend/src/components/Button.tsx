import styled from 'styled-components';

const Button = styled.button`
    height: 1.8rem;
    border-radius: 0.25rem;
    background-color: #2a6bff;
    color: #fff;
    border: 1px solid #e5ecf5;
    padding: 0.5em 2em;
    cursor: pointer;
`;

export default Button;
