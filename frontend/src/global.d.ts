type RFCDate = string
type RepoURL = string

type Commit = {
    hash: string
    author: string
    email: string
    message: string
    when: RFCDate
}