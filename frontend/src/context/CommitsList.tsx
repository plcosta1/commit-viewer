import React, { useEffect, useReducer } from 'react';
import fetchCommits from '../api/fetchCommits';

interface CLState {
    loading: boolean;
    repository: RepoURL;
    commits: Commit[];
    error: string | null;
    complete: boolean,
}

type CLAction =
 | { type: 'change_repo', value: string }
 | { type: 'requesting' }
 | { type: 'load_success', value: Commit[] }
 | { type: 'page_success', value: Commit[] }
 | { type: 'request_failed', error: string };

 interface CommitListContext extends CLState {
    changeRepo: (r: string) => void;
    loadMore: () => void;
}

const CommitListContext = React.createContext<CommitListContext>({} as any);
CommitListContext.displayName = 'CommitListContext';

function initState(repo: string): CLState {
    return {
        loading: true,
        repository: repo,
        commits: [],
        error: null,
        complete: false,
    };
}

const reducer = (state: CLState, action: CLAction) => {
    switch(action.type){
        case 'change_repo':
            return initState(action.value);
        case 'requesting':
            return {...state, loading: true }
        case 'load_success':
            return {...state, loading: false, commits: action.value}
        case 'page_success': {   
            const ids = new Set(state.commits.map(d => d.hash));
            const merged = [...state.commits, ...action.value.filter(d => !ids.has(d.hash))];
            return {...state, loading: false, commits: merged, complete: merged.length === state.commits.length }
        }
        case 'request_failed':
            return {...state, loading: false, error: action.error }
        default:
            return state
    }
}

const CommitListProvider: React.FC<{defaultRepo: string}> = ({children, defaultRepo}) => {
    const [state, dispatch] = useReducer(reducer, defaultRepo, initState);
    const { commits, repository } = state;
    const latestSha = commits.length !== 0 ? commits[commits.length-1].hash : ""

    const loadMore = () => {
        dispatch({ type: 'requesting'})
        try{
            fetchCommits(repository, latestSha).then((data) => {
                dispatch({ type: 'page_success', value: data})
            }).catch((e) => {
                dispatch({ type: 'request_failed', error: e.message})
            });
        } catch(e) {
            dispatch({ type: 'request_failed', error: 'Unknown error'})
        }
    };

    const changeRepo = async (repo: string) => {
        dispatch({ type: 'change_repo', value: repo })
        try{
            fetchCommits(repo, "").then((data) => {
                dispatch({ type: 'load_success', value: data })
            }).catch((e) => {
                dispatch({ type: 'request_failed', error: e.message})
            });
        } catch(e) {
            dispatch({ type: 'request_failed',  error: 'Unknown error'})
        }
    }

    useEffect(() => {
        changeRepo(defaultRepo)
    }, [defaultRepo])


    const val = {
        ...state,
        loadMore,
        changeRepo,
    }

    return (
        <CommitListContext.Provider value={val}>
            {children}
        </CommitListContext.Provider>
    )
}

export { CommitListProvider }
export default CommitListContext;