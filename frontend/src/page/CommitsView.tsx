import React, { useState, useContext, useRef, useEffect } from 'react';
import styled from 'styled-components';
import CommitListContext from '../context/CommitsList'
import CommitsTable from '../components/CommitsTable';
import InputField from '../components/InputField';
import Button from '../components/Button';

const ControlsContainer = styled.div`
    input{
        margin-right: 0.5rem;
    }
`;

const LoadTrigger = styled.div`
    margin: auto;
    width: 15%;
    height: 2rem;
`;

const ErrorText = styled.div`
    color: red;
    font-size: 0.6em;
`;

const LoaderText = styled.div`
    text-align: center;
    color: #15294a;
    padding: 0.5rem;
`;

const useTriggerOnView = (cb: () => void) => {
    const ref = useRef<HTMLDivElement>(null);

    useEffect(() => {
        const handleScroll = (e: any) => {
            if(!ref.current) return;
            const { offsetTop } = ref.current;
            if(window.pageYOffset + window.innerHeight > offsetTop) {
                cb();
            }
        }
        window.addEventListener('scroll', handleScroll);
        return () => window.removeEventListener('scroll', handleScroll);
    }, [cb]);

    return ref
}

const CommitViewPage = () => {
    const { repository, commits, loading, changeRepo, loadMore, error, complete } = useContext(CommitListContext)
    const [input, setInput] = useState(repository)

    const loadRepo = () => {
        if (loading) return;
        changeRepo(input)
    }
    
    const ref = useTriggerOnView(loadMore);

    return (
        <div>
            <ControlsContainer>
                <InputField value={input} placeholder="Repository's URL" onChange={(e) => setInput(e.target.value)}/>
                <Button type="submit" onClick={loadRepo}>Load</Button>
                {error && (
                    <ErrorText>{error}</ErrorText>
                )}
            </ControlsContainer>
            <h1>Commits</h1>
            <CommitsTable commits={commits} />
            {!loading && !complete && <LoadTrigger ref={ref} />}
            {loading && <LoaderText>Loading...</LoaderText>}
        </div>
    )
}

export default CommitViewPage