package commits

import (
	"encoding/json"
	"fmt"
	"time"

	"gopkg.in/src-d/go-git.v4/plumbing"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
)

type HTTPCommit struct {
	object.Commit
}

func NewHTTPCommit(c *object.Commit) *HTTPCommit {
	return &HTTPCommit{*c}
}
func NewHTTPCommitFromGH(c *GitHubResponse) *HTTPCommit {
	return &HTTPCommit{
		object.Commit{
			Hash: plumbing.NewHash(c.Sha),
			Author: object.Signature{
				Name:  c.Commit.Author.Name,
				Email: c.Commit.Author.Email,
				When:  c.Commit.Author.Date,
			},
			Message: c.Commit.Message,
		},
	}
}

func (c *HTTPCommit) MarshalJSON() ([]byte, error) {
	jsonMap := &struct {
		Hash    string    `json:"hash"`
		Author  string    `json:"author"`
		Email   string    `json:"email"`
		Message string    `json:"message"`
		When    time.Time `json:"when"`
	}{
		Hash:    c.Hash.String(),
		Author:  c.Author.Name,
		Email:   c.Author.Email,
		Message: c.Message,
		When:    c.Author.When,
	}
	return json.Marshal(&jsonMap)
}

type GithubURL struct {
	Owner string
	Repo  string
}

func (gh *GithubURL) URL() string {
	return fmt.Sprintf("https://github.com/%s/%s", gh.Owner, gh.Repo)
}
