package commits

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"

	"github.com/pkg/errors"
)

type GitHubResponse struct {
	Sha    string `json:"sha"`
	Commit struct {
		Author struct {
			Name  string    `json:"name"`
			Email string    `json:"email"`
			Date  time.Time `json:"date"`
		} `json:"author"`
		Message string `json:"message"`
	} `json:"commit"`
}

func LogFromAPI(ghurl *GithubURL, sinceSHA string) ([]*HTTPCommit, error) {
	client := http.Client{
		Timeout: time.Duration(5 * time.Minute),
	}

	query := ""
	if sinceSHA != "" {
		query = fmt.Sprintf("?sha=%s", url.QueryEscape(sinceSHA))
	}

	url := fmt.Sprintf("https://api.github.com/repos/%s/%s/commits%s", ghurl.Owner, ghurl.Repo, query)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, errors.Wrap(err, "failed to build request")
	}

	req.Header.Set("Accept", "application/vnd.github.v3+json")
	resp, err := client.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "failed to make request")
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrap(err, "failed to read response body")
	}

	data := []GitHubResponse{}
	err = json.Unmarshal(body, &data)
	if err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal response body")
	}

	commitList := []*HTTPCommit{}
	for _, ghresp := range data {
		hcommit := NewHTTPCommitFromGH(&ghresp)
		commitList = append(commitList, hcommit)
	}

	return commitList, nil
}
