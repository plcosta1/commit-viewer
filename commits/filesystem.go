package commits

import (
	"github.com/pkg/errors"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"gopkg.in/src-d/go-git.v4/storage/memory"
)

func LogFromClone(url *GithubURL, sinceSHA string) ([]*HTTPCommit, error) {
	r, err := git.Clone(memory.NewStorage(), nil, &git.CloneOptions{
		URL: url.URL(),
	})
	if err != nil {
		return nil, ErrNotFound
	}

	opt := git.LogOptions{
		Order: git.LogOrderCommitterTime,
	}
	if sinceSHA != "" {
		opt.From = plumbing.NewHash(sinceSHA)
	}
	iter, err := r.Log(&opt)
	if err != nil {
		return nil, errors.Wrap(err, "failed to log")
	}

	commitList := []*HTTPCommit{}
	for i := 0; i < 30; i++ {
		commit, err := iter.Next()
		if err != nil {
			break
		}
		hcommit := NewHTTPCommit(commit)
		commitList = append(commitList, hcommit)
	}
	iter.Close()
	return commitList, nil
}
