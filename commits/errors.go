package commits

import (
	"encoding/json"
)

type CommitListError int

const (
	ErrNotFound CommitListError = iota
	ErrInvalidURL
	ErrInvalidSHA
)

var toString = map[CommitListError]string{
	ErrNotFound:   "Repository not found",
	ErrInvalidURL: "Invalid Github URL",
	ErrInvalidSHA: "Invalid SHA param",
}

func (e CommitListError) Error() string {
	return toString[e]
}

func (e CommitListError) MarshalJSON() ([]byte, error) {
	a := map[string]interface{}{
		"code":    int(e),
		"message": e.Error(),
	}
	return json.Marshal(&a)
}
